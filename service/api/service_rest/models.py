from django.db import models


class AutomobileVO(models.Model):
    import_href =  models.CharField(max_length=200, blank=True, null=True, unique=True)
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)

    def __str__(self):
        return self.vin

class Technician(models.Model):
    name_first = models.CharField(max_length=100)
    name_last = models.CharField(max_length=100)
    employee_number = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name_first + ' ' + self.name_last

class Appointment(models.Model):
    vin = models.CharField(max_length=100)
    customer_name = models.CharField(max_length=200)
    vip = models.BooleanField(default=False, null=True)
    date = models.CharField(max_length=15)
    time = models.CharField(max_length=15)
    reason = models.CharField(max_length=200)
    is_finished = models.BooleanField(default=False, null=True)

    technician = models.ForeignKey(
        Technician,
        related_name="appointment",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.customer_name
