from django.db import models
from django.urls import reverse

class AutomobileVO(models.Model):
    vin=models.CharField(max_length=17, unique=True)
    import_href=models.CharField(max_length=200)

    def __str__(self):
        return self.vin

class SalesPerson(models.Model):
    first_name= models.CharField(max_length=200)
    last_name=models.CharField(max_length=200)
    employee_number= models.PositiveSmallIntegerField()

    def __str__(self):
        return f"{self.first_name}"


class Customer(models.Model):
    first_name=models.CharField(max_length=200)
    last_name=models.CharField(max_length=200)
    address= models.CharField(max_length=200)
    phone_number= models.CharField(max_length=20, unique=True)

    def __str__(self):
        return self.first_name


class Sale(models.Model):
    price= models.IntegerField()
    sales_person=models.ForeignKey(
        SalesPerson,
        related_name="sales_person",
        on_delete=models.CASCADE,
    )
    customer=models.ForeignKey(
        Customer,
        related_name="customer",
        on_delete=models.CASCADE,
    )
    automobile= models.ForeignKey(
        AutomobileVO,
        related_name="automobile",
        on_delete=models.CASCADE,
    )

# Create your models here.
