from django.urls import path
from .views import api_list_customer, api_customer, api_list_salesperson, api_salesperson, api_sales, api_list_sales

urlpatterns = [
    path("customers/", api_list_customer, name="api_list_customer"),
    path("customers/<int:id>/", api_customer, name="api_customer"),
    path("salespeople/", api_list_salesperson, name="api_list_salesperson"),
    path("salespeople/<int:id>/", api_salesperson, name="api_salesperson"),
    path("sales/", api_list_sales, name="api_list_sales"),
    path("sales/<int:id>/", api_sales, name="api_sales"),
]
