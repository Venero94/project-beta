from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import SalesPerson, AutomobileVO, Customer, Sale


class AutomobileVOEncoder(ModelEncoder):
    model=AutomobileVO
    properties=[
        "vin",
        "import_href",
        "id"
    ]

class CustomerEncoder(ModelEncoder):
    model= Customer
    properties=[
        "first_name",
        "last_name",
        "address",
        "phone_number",
        "id"
    ]

class SalesPersonEncoder(ModelEncoder):
    model= SalesPerson
    properties=[
        "first_name",
        "last_name",
        "employee_number",
        "id"
    ]

class SaleEncoder(ModelEncoder):
    model=Sale
    properties=[
        "price",
        "sales_person",
        "customer",
        "automobile",
        "id"
    ]
    encoders={
        "sales_person": SalesPersonEncoder(),
        "customer":CustomerEncoder(),
        "automobile":AutomobileVOEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_customer(request):
    if request.method=="GET":
        customers= Customer.objects.all()
        return JsonResponse(
            {"customers":customers},
            encoder=CustomerEncoder
        )
    else:
        try:
            content= json.loads(request.body)
            customer=Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except:
            response=JsonResponse(
                {"message":"Invalid info. Try again."}
            )
            response.status_code=400
            return response


@require_http_methods(["DELETE"])
def api_customer(request, id):
    try:
        count, _ = Customer.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    except Customer.DoesNotExist:
        response = JsonResponse({"message": "Customer does not exist"})
        return response



@require_http_methods(["GET", "POST"])
def api_list_salesperson(request):
    if request.method=="GET":
        person=SalesPerson.objects.all()
        return JsonResponse(
            {"person":person},
            encoder=SalesPersonEncoder,
        )
    else:
        try:
            content= json.loads(request.body)
            persons=SalesPerson.objects.create(**content)
            return JsonResponse(
                persons,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except:
            response=JsonResponse(
                {"message":"Invalid info. Try again"}
            )
            response.status_code=400
            return response

@require_http_methods(["DELETE"])
def api_salesperson(request,id):
    try:
        count,_=SalesPerson.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count>0})
    except SalesPerson.DoesNotExist:
        response = JsonResponse({"message":"Sales Person does not exist"})
        return response


@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method=="GET":
        sale=Sale.objects.all()
        return JsonResponse(
            {"sale":sale},
            encoder=SaleEncoder,
            safe=False
        )
    else:
        content=json.loads(request.body)
        try:

            customer= Customer.objects.get(id=content['customer'])
            content["customer"]=customer

            automobile=AutomobileVO.objects.get(vin=content["automobile"])
            content["automobile"]=automobile

            sales_person=SalesPerson.objects.get(id=content["sales_person"])
            content["sales_person"]=sales_person
            sales=Sale.objects.create(**content)
            return JsonResponse(
                sales,
                encoder=SaleEncoder,
                safe=False
        )
        except:
            response= JsonResponse(
                {"message":"Invalid info. Try again."}
            )
            response.status_code=400
            return response


@require_http_methods(["DELETE"])
def api_sales(request, id):
    try:
        count,_=Sale.objects.filter(id=id).delete()
        return JsonResponse(({"deleted": count >0}))
    except Sale.DoesNotExist:
        response= JsonResponse({"message": "Sale does not exist"})
        return response
