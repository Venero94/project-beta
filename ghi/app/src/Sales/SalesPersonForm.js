import React, {useEffect, useState} from "react"

function SalesPersonForm(){
  const [firstName, setFirstName]=useState('')
  const [lastName, setLastName]=useState('')
  const [employeeNumber, setEmployeeNumber]=useState('')


  const handleFirstChange=(event)=>{
    const value=event.target.value
    setFirstName(value)
  }

  const handleLastChange=(event)=>{
    const value=event.target.value
    setLastName(value)
  }

  const handleEmployeeNumberChange=(event)=>{
    const value=event.target.value
    setEmployeeNumber(value)
  }

  const handleSubmit=async(event)=>{
    event.preventDefault()
    const data={}
    data.first_name=firstName
    data.last_name=lastName
    data.employee_number=employeeNumber


    const url='http://localhost:8090/api/salespeople/'
    const fetchConfig={
      method:"post",
      body:JSON.stringify(data),
      headers:{
        "Content-Type":"application/json"
      }
    }
    const response= await fetch(url, fetchConfig)
    if (response.ok){
      const newCustomer=await response.json()

      setFirstName('')
      setLastName('')
      setEmployeeNumber('')
    }
  }
    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>New Sales Person</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleFirstChange} value={firstName} placeholder="first_name" required type="text" name="first_name" id="first_name" className="form-control"/>
                <label htmlFor="first_name">First Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleLastChange} value={lastName} placeholder="last_name" required type="text" name="last_name" id="last_name" className="form-control"/>
                <label htmlFor="last_name">Last Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEmployeeNumberChange} value={employeeNumber} placeholder="employee_number" required type="number" name="employee_number" id="employee_number" className="form-control"/>
                <label htmlFor="employee_number">Employee Number</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}
export default SalesPersonForm
