import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";

function SaleRecordList(){
  const [sale, setSale]=useState([])
  const [salesPerson, setSalesPerson]=useState([])

  const fetchData=async()=>{
    const response = await fetch('http://localhost:8090/api/sales/')

//
    if (response.ok){
      const data= await response.json()
      setSale(data.sale)
    }


    const personResponse=await fetch("http://localhost:8090/api/salespeople/")
    if (personResponse.ok){
      const personData=await personResponse.json()
      setSalesPerson(personData.person)
    }
  }
    useEffect(()=>{
      fetchData()
    },[])

    return (
      <>
      <h1>Sales Record List</h1>
      <div className="d-flex justify-content-end">
        <NavLink className="nav-link" to="/sales/new">
          <button type="button" className="btn btn-success">Create a Sale</button>
        </NavLink>
      </div>
        <table className="table table-striped">
            <thead>
              <tr>
                <th>Sales Person</th>
                <th>Employee id</th>
                <th>Customer</th>
                <th>VIN</th>
                <th>Price</th>
              </tr>
            </thead>
            <tbody>
              {sale?.map(sales => {
                return (
                  <tr key={sales.id}>
                    <td>{sales.sales_person.first_name}</td>
                    <td>{sales.sales_person.employee_number}</td>
                    <td>{sales.customer.first_name}</td>
                    <td>{sales.automobile.vin}</td>
                    <td>{sales.price}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
      </>
    )
  }

  export default SaleRecordList;
