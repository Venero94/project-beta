import React, {useEffect, useState} from "react";

function VehicleForm(){
  const [modelName, setModelName]=useState('')
  const [pictureUrl, setPictureUrl]=useState('')
  const [manufacturer, setManufacturer]=useState('')
  const [manufacturers, setManufacturers]=useState([])

  const handleNameChange=(event)=>{
    const value= event.target.value
    setModelName(value)
  }

  const handlePictureChange=(event)=>{
    const value= event.target.value
    setPictureUrl(value)
  }

  const handleManChange=(event)=>{
    const value= event.target.value
    setManufacturer(value)
  }

  const handleSubmit=async(event)=>{
    event.preventDefault()
    const data={}
    data.name= modelName
    data.picture_url=pictureUrl
    data.manufacturer_id=manufacturer


    const url='http://localhost:8100/api/models/'
    const fetchConfig={
      method:"post",
      body: JSON.stringify(data),
      headers: {
        "Content-type":"application/json"
      }
    }
    const response = await fetch(url, fetchConfig)

    if (response.ok) {
      const newVehicle=await response.json()

      setModelName('')
      setPictureUrl('')
      setManufacturer('')
    }
  }

  const fetchData=async()=>{
    const url="http://localhost:8100/api/manufacturers/"
    const response=await fetch(url)
    if (response.ok){
      const data = await response.json()
      setManufacturers(data.manufacturers)
    }
  }
  useEffect(()=>{
    fetchData()
  }, [])
    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>New Vehicle</h1>
            <form onSubmit={handleSubmit} id="create-vehicle-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={modelName} placeholder="model_name" required type="text" name="model_name" id="model_name" className="form-control"/>
                <label htmlFor="model_name">Model Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePictureChange} value={pictureUrl} placeholder="picture_url" required type="text" name="picture_url" id="picture_url" className="form-control"/>
                <label htmlFor="picture_url">Picture Url</label>
              </div>
              <div className="mb-3">
                <select onChange={handleManChange} value={manufacturer} placeholder="manufacturer" name="manufacturer" required id="manufacturer" className="form-select">
                <option value=" " >Choose a Manufacturer</option>
                {manufacturers.map(manufacturer =>{
                  return(
                    <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                  )
                })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default VehicleForm
