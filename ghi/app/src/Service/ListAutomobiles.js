import React, { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';

function ListAutomobiles() {
    const [Automobiles, setAutomobiles] = useState([])

    useEffect(()=> {
        getAutomobiles();
    },[])

    async function getAutomobiles() {
        const response = await fetch('http://localhost:8100/api/automobiles/')
        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data);
        }
    }

  return (
      <>
        <h1>Automobiles</h1>
        <div className="d-flex justify-content-end">
            <NavLink className="nav-link" to="/automobiles/new">
                <button type="button" className="btn btn-success">Create an automobile</button>
            </NavLink>
        </div>
        <table className="table table-striped">
        <thead>
            <tr>
            <th>Model</th>
            <th>Manufacturer</th>
            <th>Year</th>
            <th>Color</th>
            <th>VIN</th>
            <th>Sold</th>
            </tr>
        </thead>
        <tbody>
            {Automobiles.autos?.map(automobile => {
            return (
                <tr key={automobile.id}>
                <td>{ automobile.model.name }</td>
                <td>{ automobile.model.manufacturer.name }</td>
                <td>{ automobile.year }</td>
                <td>{ automobile.color }</td>
                <td>{ automobile.vin }</td>
                {automobile.sold && <td>Y</td>}
                {!automobile.sold && <td>N</td>}
                </tr>
            );
            })}
        </tbody>
        </table>
      </>
  );
}

export default ListAutomobiles;
