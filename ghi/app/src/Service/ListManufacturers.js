import React, { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';

function ListManufacturers() {
    const [Manufacturers, setManufacturer] = useState([])

    useEffect(()=> {
        getManufacturers();
    },[])

    async function getManufacturers() {
        const response = await fetch('http://localhost:8100/api/manufacturers/')
        if (response.ok) {
            const data = await response.json();
            setManufacturer(data);
        }
    }

  return (
      <>
        <h1>Manufacturers</h1>
        <div className="d-flex justify-content-end">
            <NavLink className="nav-link" to="/manufacturers/create">
                <button type="button" className="btn btn-success">Create a manufacturer</button>
            </NavLink>
        </div>
        <table className="table table-striped">
        <thead>
            <tr>
            <th>Manufacturer Name</th>
            </tr>
        </thead>
        <tbody>
            {Manufacturers.manufacturers?.map(manufacturer => {
            return (
                <tr key={manufacturer.id}>
                <td>{ manufacturer.name }</td>
                </tr>
            );
            })}
        </tbody>
        </table>
      </>
  );
}

export default ListManufacturers;
