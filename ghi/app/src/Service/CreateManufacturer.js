import React, { useState } from 'react';

function CreateManufacturer(props) {
    const [manufacturerName, setManufacturerName] = useState("");

    async function handleSubmit(event) {
      event.preventDefault();
      const data = {
        name: manufacturerName,
      };


      const locationUrl = 'http://localhost:8100/api/manufacturers/';
      const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const response = await fetch(locationUrl, fetchConfig);
      if (response.ok) {
        await response.json();
        setManufacturerName;

        const successMessage = document.createElement('div');
        successMessage.innerHTML = 'Submittion Successful! Jumping to Manufacturer List now';
        successMessage.style.color = 'red';
        document.getElementById('create-manufacturer-form').appendChild(successMessage);
        setTimeout(() => {
          window.location.href = "http://localhost:3000/manufacturers/";
        }, 3000);
      }
    }

    function handleChangeManufacturerName(event) {
      const value = event.target.value;
      setManufacturerName(value);
    }

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow-lg p-4 mt-4 border border-success">
            <h1>Create a new Manufacturer</h1>
            <form onSubmit={handleSubmit} id="create-manufacturer-form"  className="text-center" >
              <div className="form-floating mb-3">
                <input onChange={handleChangeManufacturerName} value={manufacturerName} placeholder="ManufacturerName" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name_first">Manufacturer Name</label>
              </div>
              <button className="btn btn-success">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }

  export default CreateManufacturer;
