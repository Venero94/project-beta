import React, { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';

function ListTechnicians() {
    const [technicians, setTechnician] = useState([])

    useEffect(()=> {
        getTechnicians();
    },[])

    async function getTechnicians() {
        const response = await fetch('http://localhost:8080/api/technicians/')
        if (response.ok) {
            const data = await response.json();
            setTechnician(data);
        }
    }

  return (
      <>
        <h1>Technicians</h1>
        <div className="d-flex justify-content-end">
            <NavLink className="nav-link" to="/technicians/create">
                <button type="button" className="btn btn-success">Create a technician</button>
            </NavLink>
        </div>
        <table className="table table-striped">
        <thead>
            <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Employee Number</th>
            <th>ID#</th>
            </tr>
        </thead>
        <tbody>
            {technicians.technicians?.map(technician => {
            return (
                <tr key={technician.id}>
                <td>{ technician.name_first }</td>
                <td>{ technician.name_last }</td>
                <td>{ technician.employee_number }</td>
                <td>{ technician.id }</td>
                </tr>
            );
            })}
        </tbody>
        </table>
      </>
  );
}

export default ListTechnicians;
