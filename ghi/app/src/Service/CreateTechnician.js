import React, { useState } from 'react';

function CreateTechnician(props) {
    const [nameFirst, setNameFirst] = useState('');
    const [nameLast, setNameLast] = useState('');
    const [employeeNumber, setEmployeeNumber] = useState('');

    async function handleSubmit(event) {
      event.preventDefault();
      const data = {
        name_first: nameFirst,
        name_last: nameLast,
        employee_number: employeeNumber,
      };


      const locationUrl = 'http://localhost:8080/api/technicians/';
      const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const response = await fetch(locationUrl, fetchConfig);
      if (response.ok) {
        await response.json();
        setNameFirst('');
        setNameLast('');
        setEmployeeNumber('');

        const successMessage = document.createElement('div');
        successMessage.innerHTML = 'Submittion Successful! Jumping to Technician List now';
        successMessage.style.color = 'red';
        document.getElementById('create-technician-form').appendChild(successMessage);
        setTimeout(() => {
          window.location.href = "http://localhost:3000/technicians/";
        }, 3000);
      }
    }

    function handleChangeNameFirst(event) {
      const value = event.target.value;
      setNameFirst(value);
    }

    function handleChangeNameLast(event) {
      const value = event.target.value;
      setNameLast(value);
    }

    function handleChangeEmployeeNumber(event) {
      const value = event.target.value;
      setEmployeeNumber(value);
    }

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow-lg p-4 mt-4 border border-success">
            <h1>Create a new Technician</h1>
            <form onSubmit={handleSubmit} id="create-technician-form"  className="text-center" >
              <div className="form-floating mb-3">
                <input onChange={handleChangeNameFirst} value={nameFirst} placeholder="FirstName" required type="text" name="name_first" id="name_first" className="form-control" />
                <label htmlFor="name_first">First Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleChangeNameLast} value={nameLast} placeholder="LastName" required type="text" name="name_last" id="name_last" className="form-control" />
                <label htmlFor="name_last">Last Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleChangeEmployeeNumber} value={employeeNumber} placeholder="EmployeeNumber" required type="text" name="employee_number" id="employee_number" className="form-control" />
                <label htmlFor="employee_number">Employee Number</label>
              </div>
              <button className="btn btn-success">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }

  export default CreateTechnician;
